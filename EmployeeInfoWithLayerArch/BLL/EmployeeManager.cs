﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeInfoWithLayerArch.DAL.DAO;
using EmployeeInfoWithLayerArch.DAL.Gateway;

namespace EmployeeInfoWithLayerArch.BLL
{
    class EmployeeManager
    {
        private  EmployeeGateway employeeGateway = new EmployeeGateway();

        public string Save(Employee anEmployee)
        
        {
            if (employeeGateway.HasEmployeeWithEmail(anEmployee.Email))
            {
                throw new Exception("Your system already has an Employee with this email address. Try again");
            }
            else
            {
                return employeeGateway.Save(anEmployee);
            }
        
        
        }

        public string Update(Employee anEmployee)
        {
            Employee selectedEmployee = employeeGateway.FindEmployeeEmail(anEmployee.Email);
            if (selectedEmployee.ID != anEmployee.ID)
            {
                return "Your system already has an Employee with this email address. Try again";
            }
            else
            {
                return employeeGateway.Update(anEmployee);
            }

        }

        public string DeleteEmployee(Employee selectedEmployee)
        {
            return employeeGateway.Delete(selectedEmployee);
        }

        public List<Employee> GetAllEmployees(string name)
        {
            return employeeGateway.GetAllEmployee(name);
        }
    }
}
