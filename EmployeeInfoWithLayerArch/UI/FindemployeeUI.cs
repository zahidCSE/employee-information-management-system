﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeeInfoWithLayerArch.BLL;
using EmployeeInfoWithLayerArch.DAL.DAO;

namespace EmployeeInfoWithLayerArch.UI
{
    public partial class FindemployeeUI : Form
    {
        private EmployeeManager employeeManager = new EmployeeManager();

        public FindemployeeUI()
        {
            InitializeComponent();
        }

        private string partialName = "";
        private void searchButton_Click(object sender, EventArgs e)
        {
            partialName = searchTextBox.Text;
            LoadListView(partialName);
        }


        public void LoadListView(string partialName)
        {
            resultListView.Items.Clear();
            List<Employee> employees = employeeManager.GetAllEmployees(partialName);
            if (employees.Count > 0)
            {
                int serial = 1;
                foreach (Employee employee in employees)
                {
                    ListViewItem anItem = new ListViewItem(serial.ToString());
                    anItem.Tag = (Employee) employee;
                    anItem.SubItems.Add(employee.Name);
                    anItem.SubItems.Add(employee.Email);
                    resultListView.Items.Add(anItem);
                    serial++;
                }
            }
            else
            {
                MessageBox.Show(@"Employee with given searching criteria is not found in the system", @"Information",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            editToolStripMenuItem.Enabled = (resultListView.Items.Count > 0);
            deleteToolStripMenuItem.Enabled = (resultListView.Items.Count > 0);
        }


        private Employee GetSelectedEmployee()
        {
            int index = resultListView.SelectedIndices[0];
            ListViewItem item = resultListView.Items[index];
            return (Employee) item.Tag;

        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Employee selectedEmployee = GetSelectedEmployee();

            if (selectedEmployee != null)
            {
                EntryUI employeeEntryUi = new EntryUI(selectedEmployee);
                employeeEntryUi.ShowDialog();
                LoadListView(partialName);
                resultListView.HideSelection = false;
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Employee selectedEmployee = GetSelectedEmployee();

            int index = resultListView.SelectedIndices[0];
            DialogResult result = MessageBox.Show("You are about to Delete " + selectedEmployee.Name + "\nIs that allright?", "Delete Employee", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                employeeManager.DeleteEmployee(selectedEmployee);
                resultListView.Items.RemoveAt(index);
            }

        }



    }
}
