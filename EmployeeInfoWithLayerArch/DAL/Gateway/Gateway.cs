﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeInfoWithLayerArch.DAL.Gateway
{
    public class Gateway
    {
        private SqlConnection connectionObj;
        private SqlCommand commandObj;

        public Gateway()
        {
            connectionObj= new SqlConnection(ConfigurationManager.ConnectionStrings["connectionDB"].ConnectionString);
       
            commandObj=new SqlCommand();
        }

        public SqlConnection sqlConnectionLink
        {
            get { return connectionObj; }
        }

        public SqlCommand commandObjQuery
        {
            get
            {
                commandObj.Connection = connectionObj;
                return commandObj;
            }
        }
    }
}
