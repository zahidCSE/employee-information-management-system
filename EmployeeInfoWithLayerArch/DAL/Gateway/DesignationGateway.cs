﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using EmployeeInfoWithLayerArch.DAL.DAO;

namespace EmployeeInfoWithLayerArch.DAL.Gateway
{
    class DesignationGateway
    {
         Gateway newGateway = new Gateway();

        public List<Designation> GetAll()
        {
            newGateway.commandObjQuery.CommandText = "SELECT * FROM Designation_tbl";
            newGateway.sqlConnectionLink.Open();
            SqlDataReader reader = newGateway.commandObjQuery.ExecuteReader();
            List<Designation> senderList= new List<Designation>();
            while (reader.Read())
            {
                Designation newList =new Designation();
                newList.ID = int.Parse(reader[0].ToString());
                newList.Code = reader[1].ToString();
                newList.Title = reader[2].ToString();
                senderList.Add(newList);

            }
            reader.Close();
            newGateway.sqlConnectionLink.Close();
            return senderList;

        }

        public bool HasDesignationTitle(string title)
        {
            newGateway.commandObjQuery.CommandText = "SELECT * FROM Designation_tbl WHERE Title='" + title + "'";
            newGateway.sqlConnectionLink.Open();
            SqlDataReader reader = newGateway.commandObjQuery.ExecuteReader();
            bool result = false;
            while (reader.Read())
            {
                result = true;
                break;
            }
            reader.Close();
            newGateway.sqlConnectionLink.Close();
            return result;

        }
        public bool HasDesignationCode(string code)
        {
            newGateway.commandObjQuery.CommandText = "SELECT * FROM Designation_tbl WHERE Code='" + code + "'";
            newGateway.sqlConnectionLink.Open();
            SqlDataReader reader = newGateway.commandObjQuery.ExecuteReader();
            bool result = false;
            while (reader.Read())
            {
                result = true;
                break;
            }
            reader.Close();
            newGateway.sqlConnectionLink.Close();
            return result;

        }

        public void Save(Designation aDesignation)
        {
            newGateway.commandObjQuery.CommandText = "insert into Designation_tbl(Code,Title) values('" +     aDesignation.Code + "','" + aDesignation.Title + "')";
            newGateway.sqlConnectionLink.Open();
            newGateway.commandObjQuery.ExecuteNonQuery();
            newGateway.sqlConnectionLink.Close();

        }

        public Designation GetDesignation(int designationId)
        {
            newGateway.sqlConnectionLink.Open();
            string query = String.Format("SELECT * FROM Designation_tbl WHERE Id='{0}'", designationId);
            newGateway.commandObjQuery.CommandText = query;
            SqlDataReader reader = newGateway.commandObjQuery.ExecuteReader();
            Designation aDesignation = new Designation();
            while (reader.Read())
            {
                aDesignation.ID=Convert.ToInt32(reader["ID"]);
                aDesignation.Code = reader["Code"].ToString();
                aDesignation.Title = reader["Title"].ToString();
            }
            newGateway.sqlConnectionLink.Close();
            return aDesignation;
        }
    }
}
