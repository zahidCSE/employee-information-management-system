﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeInfoWithLayerArch.DAL.DAO;

namespace EmployeeInfoWithLayerArch.DAL.Gateway
{
    internal class EmployeeGateway
    {
        private DesignationGateway designationGateway = new DesignationGateway();

        private Gateway newGateway = new Gateway();


        public string Save(Employee anEmployee)
        {
            string message = "";
            newGateway.sqlConnectionLink.Open();
            string query = String.Format("INSERT INTO Employee_tbl VALUES('{0}','{1}','{2}','{3}')", anEmployee.Name,
                anEmployee.Email, anEmployee.Address, anEmployee.aDesignation.ID);
            newGateway.commandObjQuery.CommandText = query;
            newGateway.commandObjQuery.ExecuteNonQuery();
            newGateway.sqlConnectionLink.Close();
            message = "Employee: " + anEmployee.Name + " has been saved";
            return message;
        }

        public Employee FindEmployeeEmail(string email)
        {
            newGateway.sqlConnectionLink.Open();
            string query = String.Format("SELECT * FROM Employee_tbl WHERE Email='{0}'", email);
            newGateway.commandObjQuery.CommandText = query;
            SqlDataReader reader = newGateway.commandObjQuery.ExecuteReader();

            if (reader != null)
            {
                while (reader.Read())
                {
                    Employee aEmployee = new Employee();
                    aEmployee.ID = Convert.ToInt16(reader["ID"]);
                    aEmployee.Name = reader["Name"].ToString();
                    aEmployee.Email = reader["Email"].ToString();
                    aEmployee.Address = reader["Address"].ToString();
                    aEmployee.aDesignation = designationGateway.GetDesignation(Convert.ToInt16(reader["DesignationID"]));
                    newGateway.sqlConnectionLink.Close();
                    return aEmployee;

                }
            }
            newGateway.sqlConnectionLink.Close();
            return null;

        }

        public bool HasEmployeeWithEmail(string email)
        {
            if (FindEmployeeEmail(email) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public List<Employee> GetAllEmployee()
        {
            string nameOfName = "";
            return GetAllEmployee(nameOfName);
        }

        public List<Employee> GetAllEmployee(string partOfName)
        {
            List<Employee> employees = new List<Employee>();
            newGateway.sqlConnectionLink.Open();

            string query = String.Format("SELECT * FROM Employee_tbl");
            if (partOfName != "")
            {
                query += String.Format(" WHERE Name LIKE '%{0}%'", partOfName);
            }

            query += " ORDER BY Name";

            newGateway.commandObjQuery.CommandText = query;
            SqlDataReader reader = newGateway.commandObjQuery.ExecuteReader();
            while (reader.Read())
            {
                Employee anEmployee = new Employee();
                anEmployee.ID = Convert.ToInt32(reader["ID"]);
                anEmployee.Name = reader["Name"].ToString();
                anEmployee.Email = reader["Email"].ToString();
                anEmployee.Address = reader["Address"].ToString();
                anEmployee.aDesignation = designationGateway.GetDesignation(Convert.ToInt16(reader["DesignationID"]));
                employees.Add(anEmployee);
            }
            newGateway.sqlConnectionLink.Close();
            return employees;

        }


        public string Delete(Employee selectedEmployee)
        {
            string message = "";
            newGateway.sqlConnectionLink.Open();
            string query = String.Format("DELETE FROM Employee_tbl WHERE Id={0}", selectedEmployee.ID);
            newGateway.commandObjQuery.CommandText = query;
            newGateway.commandObjQuery.ExecuteNonQuery();
            newGateway.sqlConnectionLink.Close();
            message = "Employee: " + selectedEmployee.Name + " has been deleted.";
            return message;

        }


        public string Update(Employee anEmployee)
        {
            newGateway.sqlConnectionLink.Open();
            string message = "";
            string query =
                String.Format( "UPDATE Employee_tbl SET Name='{0}',Email='{1}',Address='{2}',DesignationId = {3} WHERE Id = {4}", anEmployee.Name, anEmployee.Email, anEmployee.Address, anEmployee.aDesignation.ID, anEmployee.ID);
            newGateway.commandObjQuery.CommandText = query;
            newGateway.commandObjQuery.ExecuteNonQuery();
            newGateway.sqlConnectionLink.Close();
            message = "Information has been updated";
            return message;
        }

    }


}
